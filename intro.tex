%!TEX root = POISam.tex
\vspace{-2ex}
\section{Introduction}\label{sec:intro}

Large collections of geospatial data are becoming increasingly available, such as geo-tagged micro-blogs (e.g., Twitter), urban data like real estate properties, etc. Such data are featured with both geospatial
content and other content, such as attributes, texts and photos.
%
It is useful to provide support for end-users to perform visualized exploration on such geospatial data on maps. However, it is overwhelming and meaningless to display all data or query results in a limited screen. 

Therefore, we aim to achieve two goals. The first goal is to achieve an efficient spatial object selection, namely the SOS query: given the current region of user's interest, how to efficiently select a set $S$ of $k$ objects (among all the spatial objects falling in this region), so that it meets (i) Visibility Constraint --- the distance between any two objects in $S$ is larger than a given distance threshold $\theta$, and (ii) Representative Constraint --- the aggregated similarity between $S$ and the whole spatial objects in that region is maximized.
%
We illustrate the above two features in the following example.

\vspace{-1.5ex}
\begin{example}\label{exm:sos}
Given a collection of tweets, an end-user would like to browse a small number (denoted by $k$) of representative tweets for an area on an online map. Ideally, the set of selected tweets can well represent the tweets in the area (i.e., Representativeness Constraint), and they should not be too close to each other so that they will not overlap with each other on the map (i.e., Visibility Constraint) when shown on the screen.
{
% Figure~\ref{fig:intro} demonstrates Example~\ref{exm:sos}, where a small number of representative POIs are shown to user (Figure~\ref{fig:intro_exp_2}).
% The user may be interested in some POIs, and she may click one to check more information such as descriptions and user's voting score.
% Moreover, the ``hidden'' POIs that are represented by the user-viewing POI are also highlighted (Figure~\ref{fig:intro_exp_3}) as related recommendations for users to explore.
Figure~\ref{fig:demo_2} demonstrates Example~\ref{exm:sos}, where a small number of representative tweets are shown to the user (Figure~\ref{fig:tweet_b}).
Note that, without selection, it looks like Figure~\ref{fig:tweet_a}.
The user may be interested in some tweets, and he may click one to check the detailed information; and the ``hidden'' tweets that are represented by the selected tweet will be listed and summarized by a word cloud for the user to further explore.}
\end{example}
\vspace{-1.5ex}

\begin{figure*}[ht]
% \vspace{-2ex}
	\centering
	{\subfigure[Wihtout any selection, it is messy to view  all the tweets on the map.]{\label{fig:tweet_a}
		\includegraphics[width=0.266\textwidth]{figure/twitter_a.pdf}
	}}\hspace{1.5ex}
	{\subfigure[Only a few representative tweets are shown to the user at the map  view (b-1). After the user clicks on a tweet near \textit{Fedration Square}, a word cloud view (b-2) summaries all the similar tweets and a detailed tweet view (b-3) is shown to the user.]{\label{fig:tweet_b}
		\includegraphics[width=0.39\textwidth]{figure/twitter_b.pdf}
	}}\hspace{1.5ex}
	{\subfigure[Illustration of the result at the map view after user panning (\textit{scroll-down} from Figure \ref{fig:tweet_b}): new tweets are shown in red.]{\label{fig:tweet_c}
		\includegraphics[width=0.266\textwidth]{figure/twitter_c.pdf}
	}}%\hspace{2.5ex}
	\vspace{-2.5ex}
	\caption{Spatial Object Selection Examples (Twitter data)}
	\vspace{-3ex}
\end{figure*}\label{fig:demo_2}

By reviewing the literature in the areas of cartographic selection and spatial sampling, we find that there have been some studies~\cite{kefaloukos2014declarative,nutanong2012multiresolution,peng2014viewing} taking the visibility constraint into account. However, none of them considers representativeness except for a study done by Drosou et al. \cite{drosou2012disc}, in which it is assumed that the representativeness of a spatial object is based on its spatial distance to other objects, and its proposed solution is built based on this assumption.

The POISam system distinguishes from the literature in two-folds. First, it supports various types of data resources, and the users can define their personalized similarity metric to meet different needs. 
% For example, when exploring properties the user can specify their preference as ``distance'' such that properties are close in a neighborhood will be hidden because of the redundancy.
For example, as shown in Figure~\ref{fig:property}, there are different attributes associated with each real estate property, and users can choose attributes based on their own preferences to define the similarity metric.
%
Second, POISam takes the importance of different objects as a factor, such that the results shown on map are highly related to the user's preference. For example, when exploring properties users can choose options like ``Distance to nearest shopping center'' or ``land size'' such that different needs can be met. User can also specify the number of shown results $k$ and the minimum overlapping threshold $\theta$ such that the objects can be displayed in the best experience.

Our second goal is to further extend the SOS query to achieve an interactive spatial object selection, in response to three common map navigation operations, i.e., zoom-in, zoom-out and panning. We define the zooming consistency and panning consistency constraint when selecting a new set of representative objects for the new map region, and formalize it as the Interactive SOS query (ISOS).

The ISOS query drops a common assumption from all previous work, i.e., the zoom levels and region cells are pre-defined and indexed, and objects are selected from such cells at a particular zoom level rather than from the user's current region of interest (which in most cases do not correspond to the pre-defined cells). It poses a challenge in object selection via online computation. 

Section~\ref{sec:model} formally defines the SOS and ISOS query, whose scenarios are demonstrated in Section~\ref{sec:demo}, where two real-world datasets from Twitter and Australia Real Estate are used. The problem of solving \SOS/\ISOS queries can be shown to be NP-hard. Hence, we devise two approximation algorithms with provable performance guarantees. The detailed algorithms to support these two goals can be found in our recent research work \cite{POISam}.

% \begin{figure*}[ht]

% 	\centering
% 	{\subfigure[Without any selection, it is messy to view all the POIs.]{\label{fig:intro_exp_1}
% 		\includegraphics[scale=0.25]{figure//intro_exp_1}
% 	}}\hspace{2.5ex}
% 	{\subfigure[After objects selection, only few representative POIs are shown to users.]{\label{fig:intro_exp_2}
% 		\includegraphics[scale=0.25]{figure//intro_exp_2}
% 	}}\hspace{2.5ex}
% 	{\subfigure[The user can click one for more detailed information, and other ``hidden'' similar POIs will also be highlighted.]{\label{fig:intro_exp_3}
% 		\includegraphics[scale=0.25]{figure//intro_exp_3}
% 	}}\vspace{-2ex}
% 	\caption{Spatial Object Selection Example {\color{red}(To be replaced by demo examples)}}
% \end{figure*}\label{fig:intro}



% {\color{red}Two realworld datasets collected from Twitter (http://www.twitter.com) and Australia Real Estate (http://realestate.com.au) (@Tao:Not sure if it is correct) are used to show both the effectiveness and efficiency of this system.}

\iffalse
The rest of the demonstration proposal is organized as follows.
Section~\ref{sec:model} introduces the formal definitions of geospatial objects and \SOS/\ISOS query. Section~\ref{sec:proto} presents the prototype of POISam. Finally Section~\ref{sec:demo} illustrates the demonstration details.
\fi